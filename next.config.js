// next.config.js
const crypto = require('crypto');

module.exports = {
	env: {
		name: "Blog", //Name that will appear in the top left
		description: "This is a blog written in Next Js", //The description of your blog
		keywords: "Blog,NextJS,js,mongodb", //The default keywords for your site
		mongodbUrl: 'mongodb://localhost:27017/', //Mongodb host URL
		host: "http://localhost:3000", //Host name of the server. Set to http://localhost:3000
		db: 'blog', //Data where the blog post are stored
		postCollection: 'blog', //Collection where documents are stored
		userCollection: 'users', //Collection where accounts are stored
		accountCreation: false, //Set to true if you want to allow new accounts to be created
		showLogin: true, //Set to true to if you want to show the login button
		jwtSecret: crypto.randomBytes(21).toString('hex') //JWT secret generated at start for signing keys
	}
};
