# Install Guide

Good Luck. I hope you enjoy!

## Requirements

- Linux/Mac OS/Windows with unix terminal
- nodejs/npm
- Mongodb Server

## Installing requirements

Based on what OS you have install NodeJS and Mongodb

### Arch Linux

- [NodeJS](https://wiki.archlinux.org/index.php/Node.js)
- [Mongodb](https://wiki.archlinux.org/index.php/MongoDB)

### Ubuntu Linux

- [NodeJS](https://tecadmin.net/install-latest-nodejs-npm-on-ubuntu/)
- [Mongodb](https://tecadmin.net/install-mongodb-on-ubuntu/)

### Mac OS

- [NodeJS](https://tecadmin.net/install-node-js-npm-macos/)
- [Mongodb](https://tecadmin.net/install-mongodb-macos/)

### Windows

- [NodeJS and Mongodb](https://www.learn2crack.com/2014/04/setup-node-js-and-mongodb.html)

## Configuring Mongodb

Using your favorite mongodb client create a database called 'blog'. Within that database create two collections called 'blog' and 'users'.

## Setting the server

In your terminal go to the root folder of this project and run `npm install`. Afterwords open the file called next.config.js in your favorite editor. These are all the variables that are used to configure the server.

- Set 'name' to the name of your blog
- Set 'mongodb' to your mongodb connection string (including the password)
- Set 'host' to the name of the host you are hosting you app on. (Leave as localhost if testing locally)

Now to run the server in development mode you can run `npm run dev`. Or if you want to run the server in production you can run `npm run build` then run `npm run start`. You server should now be available at http://localhost:3000/ if you are running the server locally.

### Important

The first time you run the server set the value of 'accountCreation' to true in next.config.js. Then once you are done creating your blog account set this value back to false and restart the server. This will disable the general public from creating accounts.

### Troubleshooting

If you are getting a 500 Error review your next.config.js. Make sure that your host does't end with '/'. Also if you host does not have https make sure that the host doesn't include the 's'. Also double check to make sure the password and data are correct in your mongodbUrl connection string. Also make sure mongodb is running.
