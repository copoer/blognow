# How to host your blog for free

This guide will show you how to set up custom blog software on free hosting.

## Resources:

Here are the three resources we will use.

- Blogging Program: Alii Blog
- [Server Hosting](https://zeit.co/)
- [Database hosting](https://mlab.com/)

## Step 1: Setting up Mongodb

Useful Promo Code: M1075

### Setting up Account/Cluster

1. Sign up for [mLabs](https://mlab.com/).
2. Click create your first cluster
3. Select "Developing a new app" if you get the new user popup
4. Select a free tier server from AWS, Google Cloud or Azure under Cloud Provider and Region
5. Check to make sure you have M0 Sandbox selected under Cluster tier
6. Set your cluster name to Cluster0 and leave the rest of the settings as default

### Setting up Database

1. Go to the _Database Access_ tab and Click _new user_
2. Create and add a user with read and write access
3. Go to the _Network Access_ tab and Click _add new ip address_
4. For now Click _allow access from anywhere_ and then click confirm. (You should set this to something more specific later)
5. Go to the _Cluster_ tab and click _collections_
6. Click _add my own data_ and add a database call `blog` with a collection called `blog`
7. Click the _+_ button next to `blog` database and add a collection called `users`
8. Go back to the _Cluster_ tab and click _connect_
9. Click _connect to your application_ then copy the connection string for nodejs (Save this for later)

## Step 2: Setting up the web server

### Zeit Account

1. Sign up for [Zeit](https://zeit.co/)
2. Install now on your local computer using `npm install -g now`
3. Run `now login` and enter your email
4. Check your email and click verify

### Configuring Blog/Deploying

1. Go to the folder you have the nextjs blog files stored in your terminal
2. Open the next.config.js file with your favorite editor
3. Change the value of 'mongodbUrl' to be the same as the connection url we copied earlier. Change <password> to be the password of the user we created. Near the end of the url change `/test` to `/blog`
4. Next change the value of 'host' to match this url schema `https://<foldername>.<zeit username>.now.sh` replacing folder name with the name of the of the folder you are working and zeit user name being the user name of your zeit account
5. Your file should now look like this:
```js 
// next.config.js
const crypto = require('crypto');

module.exports = {
	env: {
		name: "Blog", //Name that will appear in the top left
		mongodbUrl: 'mongodb+srv://<username>:<password>@<clustername>.mongodb.net/blog?retryWrites=true&w=majority', //Mongodb host URL
		host: "https://blog.copoer.now.sh", //Host name of the server. Set to localhost:3000
		db: 'blog', //Data where the blog post are stored
		postCollection: 'blog', //Collection where documents are stored
		userCollection: 'users', //Collection where accounts are stored
		accountCreation: true, //Set to true if you want to allow new accounts to be created
		jwtSecret: crypto.randomBytes(21).toString('hex') //JWT secret generated at start for signing keys
	}
};
``` 
6. Go back to the terminal and run `now`

Common Errors:
If you are getting a 500 Error make sure your host and mongodbUrl variables are set properly in the next.config.js file.

### Setting up blog account

1. Go to your new blog website and click the right arrow in the top right hand corner of the blog page
2. Enter a username and password and click create account
3. (Optional) You can disable the account creation feature for public by setting accountCreation to false in your local next.config.js file then redeploy the app using the `now` command. 

Your blog should now be up and running on free hosting. You can now start creating blog posts!
