# Pages/Components

### Top Bar:

- Logo/website name
- Search bar
- Login button
- Create post button (When logged in)
- Sign Out button (When logged in) 

### Index Page: /

- Displays all blog posts in most recent order
- Paging at bottom
- Search directs to filtered version of this page

### Post Page: /[id]

- Displays post converted from markdown to html
- Edit button (When Logged in)
- Delete button (When Logged in)

### Create Page: /post

- Title Form
- Markdown Body Form
- Keywords Form
- Url Form (Generates from title) (Checks to see if unique)
- Save button

### Edit Page: /[id]

- Title Form
- Markdown Body Form
- Keywords Form
- Url Form (Generates from title) (Checks to see if unique)
- Save Button

### Login: /login

- Username Form
- Password Form
- Login Button

---

# Mongodb Schemas

## Blog Collection

```json
{
_id:
url:
title:
markdown:
keywords:
author:
}
```

## Users Collection

```json
{
_id:
username:
password:
}
```

---

# File Structure

## Frontend

### Endpoints

- /index.js
- /post/[id]/index.js
- /post/index.js
- /login/index.js
- /logout/index.js

## Backend

### Endpoints

- /post/index.js
- /post/[id].js
- /get/[id].js
- /get/index.js
- /delete/[id].js
- /account/get/index.js
- /account/post/index.js

---

# Resources

If you have not worked with any of these technologies before please review their documentation.

- [ReactJS](https://reactjs.org/docs/getting-started.html)
- [NextJS](https://nextjs.org/docs)
- [NodeJS](https://nodejs.org/en/docs/)
- [Mongodb](https://docs.mongodb.com/)
- [Mongodb for NodeJS](https://mongodb.github.io/node-mongodb-native/3.2/api/)
