// Script to generate sitemap
const js2xmlparser = require("js2xmlparser");
const envi = require('../next.config.js');
const env = envi.env;
const fs = require('fs');
const MongoClient = require('mongodb').MongoClient;
const url = env.mongodbUrl;
let dbName = env.db;
const postCollection = env.postCollection.toString();
const userCollection = env.userCollection.toString();

function getAllPosts(callback) {
	MongoClient.connect(url, (err, db) => {
		if (err) return callback(null, err);
		const dbo = db.db(dbName);
		dbo.collection(postCollection).find({}).toArray(async function(err, res) {
			if (err) return callback(null, err);
			db.close();
			return callback(res, null);
		});
	});
}

getAllPosts((data, err) => {
	if (err) {
		console.log(err);
	} else {
		const pages = data;
		const date = new Date().toISOString().slice(0,10);
		let sitemap = [];
		sitemap.push({
			loc: env.host,
			lastmod: date,
			changefreq: "daily",
			priority: 1
		});
		for (const p of pages) {
			let page = {};
			page.loc = env.host + "/" + p.url;
			page.lastmod = date;
			page.changefreq = "monthly";
			page.priortiy = 0.5;
			sitemap.push(page);
		}
		sitemapXML = js2xmlparser.parse("urlset", sitemap).replace("urlset", 'urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"');
		fs.writeFileSync("public/sitemap.xml", sitemapXML);
		fs.writeFileSync("public/robots.txt", "User-agent: *\nSitemap: "+env.host+"/sitemap.xml");
	}
});

