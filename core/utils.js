/* General use utility functions */
//Vars used for session token verification
const jwt = require('jsonwebtoken');
const jwtSecret = process.env.jwtSecret.toString();
//Files required in a document
const requiredKeys = ["url", "title", "markdown", "keywords", "summary"];

//Validate Document
//Check to make sure all files have been set
export function invalidDocument(obj) {
	try {
		obj = JSON.parse(obj.toString());
	}
	catch(e) {
		return e;
	}
	if (typeof obj !== 'object') {
		return "Document must be object";
	}
	for (let key of requiredKeys) {
		if (!(key in obj) || obj[key] === "" || obj[key] === null) {
			return key + " is missing";
		}
	}
	return false;
}

//Checks for invalid json
//If JSON is valid it returns false
export function invalidJSON(obj) {
	try {
		obj = JSON.parse(obj.toString());
	}
	catch(e) {
		return e;
	}
	return false;
}

//Authentication wrapper for API endpoints that require 
//the user to be logged in
export function auth(req, res, callback) {
	let token;
	try {
		token = req.headers.cookie.replace(/(?:(?:^|.*;\s*)token\s*=\s*([^;]*).*$)|^.*$/, "$1");
		token = jwt.verify(token, jwtSecret);
	} catch (err) {
		// If the token is invalid return 401
		res.statusCode = 401;
		res.end(JSON.stringify({ error: "Unauthorized" }));
		return false;
	}
	//If the token is valid return to the api endpoint
	return callback(req, res, token);
}
