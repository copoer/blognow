/* Mongodb helper functions */
const MongoClient = require('mongodb').MongoClient;
const url = process.env.mongodbUrl;
let dbName = process.env.db;
const postCollection = process.env.postCollection.toString();
const userCollection = process.env.userCollection.toString();

module.exports = {
	//Create account
	createCollections: (callback) => {
		MongoClient.connect(url, (err, db) => {
			if (err) return callback(null, err);
			const dbo = db.db(dbName);
			dbo.createCollection(postCollection, function(err, res) {
				if (err) return callback(res, err);
			});
			dbo.createCollection(userCollection, function(err, res) {
				if (err) return callback(res, err);
			});
			db.close();
			return callback(true, null);
		});
	},

	//Create account
	createAccount: (obj, callback) => {
		MongoClient.connect(url, (err, db) => {
			if (err) return callback(null, err);
			const dbo = db.db(dbName);
			dbo.collection(userCollection).insertOne(obj, function(err, res) {
				if (err) return callback(null, err);
				db.close();
				return callback(res, null);
			});
		});
	},

	//Find an account
	getAccount: (query, callback) => {
		MongoClient.connect(url, (err, db) => {
			if (err) return callback(null, err);
			const dbo = db.db(dbName);
			dbo.collection(userCollection).findOne(query, function(err, res) {
				if (err) return callback(null, err);
				db.close();
				return callback(res, null);
			});
		});
	},

	//Create post
	createPost: (obj, callback) => {
		MongoClient.connect(url, (err, db) => {
			if (err) return callback(null, err);
			const dbo = db.db(dbName);
			dbo.collection(postCollection).insertOne(obj, function(err, res) {
				if (err) return callback(null, err);
				db.close();
				return callback(res, null);
			});
		});
	},

	//Update a posts data
	replacePost: (query, obj, callback) => {
		MongoClient.connect(url, (err, db) => {
			if (err) return callback(null, err);
			const dbo = db.db(dbName);
			dbo.collection(postCollection).replaceOne(query, obj, function(err, res) {
				if (err) return callback(null, err);
				db.close();
				return callback(res, null);
			});
		});
	},

	//Get a single document
	getPost: (query, callback) => {
		MongoClient.connect(url, (err, db) => {
			if (err) return callback(null, err);
			const dbo = db.db(dbName);
			dbo.collection(postCollection).findOne(query, function(err, res) {
				if (err) return callback(null, err);
				db.close();
				return callback(res, null);
			});
		});
	},

	//Get a multiple document  
	getPosts: (query, limit, page, callback) => {
		MongoClient.connect(url, (err, db) => {
			if (err) return callback(null, err);
			const dbo = db.db(dbName);
			if (page<1) page = 1;
			dbo.collection(postCollection).find(query).limit(limit).skip(10*(page-1)).toArray(async function(err, res) {
				if (err) return callback(null, err);
				const count = await dbo.collection(postCollection).count(query);
				db.close();
				return callback({count: count, data: res}, null);
			});
		});
	},

	//Delete a single document
	deletePost: (query, callback) => {
		MongoClient.connect(url, (err, db) => {
			if (err) return callback(null, err);
			const dbo = db.db(dbName);
			dbo.collection(postCollection).deleteOne(query, function(err, res) {
				if (err) return callback(null, err);
				db.close();
				return callback(res, null);
			});
		});
	}
};
