/* Paging Component*/
import Link from 'next/link'

const Paging = props => {
	let links = [];
	if (props.pageCount > 1) {
		for (let i = 0; i < props.pageCount; i++) {
			let num = i+1;
			num = num.toString();
			let str = num;
			if (num === props.page.toString()) {
				str = <b>{num}</b>;
			} 
			links.push(
				<Link href={`/search?page=${num}${props.queryString}`} key={num}>
				<a>{str}</a>
				</Link>)
		}
	}
	return (
		<>
		{props.pageCount > 1 &&
			<div className="container paging space-around">
			{links}
			</div>
		}
		</>
	)
};
export default Paging;
