/* Blog Editing Component */
/* Used while creating and editing blog posts */
import React from 'react'
import fetch from 'isomorphic-unfetch';
const host = process.env.host;

class EditPost extends React.Component {
	constructor(props) {
		super(props);
		// If Url set then initialise with that data.
		if (props.data) {
			this.state = {id:props.data._id,url:props.data.postUrl,title:props.data.title,markdown:props.data.markdown,keywords:props.data.keywords,summary:props.data.summary};
		} else {
			this.state = {url:"",title:"",markdown:"",keywords:"",summary:""};
		}
		this.updateTitle = this.updateTitle.bind(this);
		this.updateMarkdown = this.updateMarkdown.bind(this);
		this.updateKeywords = this.updateKeywords.bind(this);
		this.updateUrl = this.updateUrl.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.exsitingUrlId = this.exsitingUrlId.bind(this);
		this.hideAlert = this.hideAlert.bind(this);
		this.updateSummary = this.updateSummary.bind(this);
	}
	updateTitle(event) {
		this.setState({title: event.target.value});
		if (!this.state.urlSet && !this.state.id) {
			let url = this.state.title.split(" ").join("-");
			this.setState({url: url});
		}
	}
	updateMarkdown(event) {
		this.setState({markdown: event.target.value});
	}
	updateKeywords(event) {
		this.setState({keywords: event.target.value});
	}
	updateUrl(event) {
		// If id isn't set then check for uniqueness
		this.setState({urlSet: true});
		this.setState({url: event.target.value});
	}
	updateSummary(event) {
		this.setState({summary: event.target.value});
	}
	async handleSubmit(event) {
		event.preventDefault();

		// Handle URL Errors
		if (!this.state.url) {
			this.setState({editError: "You must define a url"});
		} else {
			this.setState({url: encodeURI(this.state.url)});
		}
		const post = this.state;

		// Delete meta data
		delete post.urlSet;
		delete post.error;
		delete post.editError;

		// Check for existing URLs
		const urlId = await this.exsitingUrlId();
		
		// If there is not an existing url and the id is not set
		// then it is a new post
		if (!urlId && !this.state.id) {
			const dataRes = await fetch(host+'/api/post', {
				method: 'POST',
				body: JSON.stringify(post)
			});
			const data = await dataRes.json();
			if (data.error) {
				this.setState({editError: data.error});
			} else {
				window.location = "/post/"+this.state.url;
			}
		
		// If the url does exist then it is an edit
		} else if (urlId === this.state.id || !urlId) {
			const dataRes = await fetch(host+'/api/post/'+this.state.id, {
				method: 'POST',
				body: JSON.stringify(post)
			});
			const data = await dataRes.json();
			if (data.error) {
				this.setState({editError: data.error});
			} else {
				window.location = "/post/"+this.state.url;
			}
		} else {
			this.setState({editError: "Your URL is not unique"});
		}
	}
	async exsitingUrlId() {
		const dataRes = await fetch(host+'/api/get/'+this.state.url, {
			method: 'GET'
		});
		const data = await dataRes.json();
		let response = false;
		if (data.data) response = data.data._id;
		return response; 
	}
	hideAlert() {
		this.setState({editError: false});
	}
	render() {
		let alertMessage;
		if (this.state.editError) {
			alertMessage = (
				<div className="alert">
				<span className="close-alert" onClick={this.hideAlert}>&times;</span>
				{this.state.editError}
				</div>
			)
		}
		return (
			<div className="edit-post background-fill">
			{alertMessage}
			<form className="container post-form" onSubmit={this.handleSubmit}>
			<h4>Title:</h4>
			<input className="form-item title" type="text" name="title" placeholder="My First Post" value={this.state.title} onChange={this.updateTitle}/>
			<h4>Body:</h4>
			<h4><a href="https://www.markdownguide.org/cheat-sheet">Markdown Cheat Sheet</a></h4>
			<textarea className="form-item markdown" rows="30" type="text" name="markdown" placeholder="Hello new world!" value={this.state.markdown} onChange={this.updateMarkdown}/>	
			<h4>Summary:</h4>
			<textarea className="form-item markdown" rows="3" type="text" name="summary" placeholder="What is this post about?" value={this.state.summary} onChange={this.updateSummary}/>
			<h4>Keywords:</h4>
			<input className="form-item keywords" type="text" name="keywords" placeholder="first,post,hello,world" value={this.state.keywords} onChange={this.updateKeywords}/>
			<h4>Url:</h4>
			<input className="form-item url" type="text" name="url" placeholder="my-first-post" value={this.state.url} onChange={this.updateUrl}/>
			<button className="form-item submit-button" type="submit">Save</button>
			</form>
			</div>
		)
	}
}
export default EditPost;
