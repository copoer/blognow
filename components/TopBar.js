/* TopBar Component */
import Link from 'next/link';
import React from 'react';
import { Cookies } from 'react-cookie';
import getConfig from 'next/config';
const siteName = process.env.name.toString();
const showLogin = process.env.showLogin;

const cookies = new Cookies();
class TopBar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			query: props.query,
			token: cookies.get('token') || false,
			name: siteName 
		};
		this.updateQuery = this.updateQuery.bind(this);
	}
	updateQuery(event) {
		this.setState({query: event.target.value});
	}
	render() {
		let extra;		
		if (this.state.token) {
			extra = (
				<>
				<Link href="/post">
				<a className="new-button">+</a>
				</Link>
				<Link href="/logout">
				<a className="logout">↦</a>
				</Link>
				</>
			);
		} else if (showLogin) {
			extra = (
				<>
				<Link href="/login">
				<a className="login">⇥</a>
				</Link>
				</>
			);
		}
		return (
			<div className="flex-container padding top-bar">
			<Link href="/">
			<div className="top-logo">
			<a>
			{this.state.name}
			</a>
			</div>
			</Link>
			<div className="flex-container center top-right">
			<div className="container-np top-search">
			<form className="top-right" action='/search'>
			<input className="search-input" type="text" name="query" placeholder="Search..." value={this.state.query} onChange={this.updateQuery}/>
			<button type="submit">Search</button>
			</form>
			</div>    
			{extra}
			</div>
			</div>
		)
	}
}
export default TopBar;
