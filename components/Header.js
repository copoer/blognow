/* Header Component */
import Head from 'next/head';
import getConfig from 'next/config';

const description = process.env.description.toString();
const keywords = process.env.keywords.toString();
const siteName = process.env.name.toString();
const Header = props => (
	<Head>
	<meta charSet="utf-8" />
	<title>{(props.title)?props.title:siteName}</title>
	<meta name="keywords" content={props.keywords ? props.keywords : keywords}/>
	<meta name="description" content={props.description ? props.description : description}/>
	<link href="/style.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js?skin=sunburst"></script>
	{props.customStyle &&
		<link href={props.customStyle} rel="stylesheet" />
	}
	</Head>
);
export default Header;
