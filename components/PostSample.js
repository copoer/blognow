/* Post Sample Component */
import Link from 'next/link'

const TopBar = props => (
	<Link href={`/${props.dataType}/${props.url}`}>
	<a> 
	<div className="post-sample">
	<h3>{props.title}</h3>
	<p>{props.body.substring(0, 150)}</p>
	</div>
	</a>
	</Link>
);
export default TopBar;
