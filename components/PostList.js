/* Post List Component */
import PostSample from './PostSample';

const PostList = props => (
	<div className="container post-container">
	{props.data.map(post =>(
		<PostSample dataType={post.dataType} key={post._id} title={post.title} body={post.summary} url={post.url} />
	))}
	</div>
);
export default PostList;
