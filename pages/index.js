/* Index Page */
import Header from '../components/Header'
import TopBar from '../components/TopBar'
import PostSample from '../components/PostSample'
import Paging from '../components/Paging';
import fetch from 'isomorphic-unfetch';
import React from 'react'
import Error from 'next/error'
const host = process.env.host;

const siteName = process.env.name.toString();
// Paging Information
class Index extends React.Component {
	static async getInitialProps(req) {
		// Get Posts
		const dataRes = await fetch(host+'/api/get', {
			method: 'POST',
			body: JSON.stringify({limit: 3})
		});
		const data = await dataRes.json();

		// Return data or error
		if (data.error) {
			return {error: "500 Database Error"};
		} else {
			return data.data;
		}
	}

	render() {
		if (this.props.error) return <Error statusCode={this.props.error} />;

		return (
			<div>
			<Header/>
			<div className="flex-container column center">
			<div className="index-search">
			<h1 className="logo">{siteName}</h1>
			<form  action='/search'>
			<input className="search-input" type="text" name="query" placeholder="Search..."/>
			<button type="submit">Search</button>
			</form>
			</div>
			<div className="container index-post-container">
			{this.props.data.map(post =>(
				<PostSample dataType={post.dataType} key={post._id} title={post.title} body={post.summary} url={post.url} />
			))}
			</div>
			</div>
			</div>
		)
	}
}


export default Index;
