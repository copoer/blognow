/* Create new blog post page */
import Header from '../../components/Header'
import TopBar from '../../components/TopBar'
import EditPost from '../../components/EditPost'

const Index = props => (
	<div>
	<Header title="Create New Post" />
	<TopBar query={props.query}/>
	<div className="container">
	<h1>Create Post</h1>
	<EditPost />
	</div>
	</div>
);

export default Index;
