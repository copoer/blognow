/* Blog Post Page */
/* Allows for viewing, editing and deleting a single post */
import Header from '../../../components/Header';
import TopBar from '../../../components/TopBar';
import EditPost from '../../../components/EditPost';
import Link from 'next/link';
import { Remarkable } from 'remarkable';
import fetch from 'isomorphic-unfetch';
import React from 'react';
import ErrorPage from 'next/error';
import { Cookies } from 'react-cookie';
const host = process.env.host;

const cookies = new Cookies();
class Post extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
		if (props.error) {
			this.state.error = props.error;
		} else {
			this.state = {edit: false, id: props.data._id, token: cookies.get('token') || false};
		}
		this.editPost = this.editPost.bind(this);
		this.deletePost = this.deletePost.bind(this);
	}
	static async getInitialProps(req) {
		const reqUrl = req.query.id;

		const dataRes = await fetch(host+`/api/get/${reqUrl}`, {
			method: 'GET'
		});
		const data = await dataRes.json();

		if (data.error) console.log(data.error);

		if (!data.data) {
			return {error: 404};
		} else {
			// Reset url
			data.data.postUrl = data.data.url;
			return data;
		}
	}
	editPost() {
		this.setState({edit: true});
	}
	async deletePost() {
		var result = confirm("Want to delete?");
		if (result) {
			const dataRes = await fetch(host+'/api/delete/'+this.state.id, {
				method: 'DELETE'
			});
			const data = await dataRes.json();
			if (data.error) {
				this.setState({error: 500});
			} else {
				window.location = "/";
			}
		}
	}
	getRawMarkup() {
		const md = new Remarkable({html:true,langPrefix:'prettyprint lang-',});
		return { __html: md.render(this.props.data.markdown)};
	}
	render () {
		if (this.state.error) {
			return <ErrorPage statusCode={this.state.error} />
		}
		let post;
		if (this.state.edit) {
			post = (
				<>
				<h1>Edit Post</h1>
				<EditPost data={this.props.data} />
				</>
			);
		} else {
			let editButtons = <></>; 
			let pageCount = <></>; 
			if (this.state.token) {
				editButtons = (
					<>
					<button className="edit-button" onClick={this.editPost}>Edit Post</button>
					<button className="delete-button" onClick={this.deletePost}>Delete Post</button>
					</>
				);
				pageCount = (
					<>
					Page Count: {this.props.data.pageCount}
					</>
				);
			}
			post = (
				<div className="post">
				<div className="flex-container center">
				{editButtons}
				<h1>{this.props.data.title}</h1>
				</div>
				{pageCount}
				<div className="post-container" dangerouslySetInnerHTML={this.getRawMarkup()}></div>
				</div>
			);
		}
		return (
			<>
			<Header title={this.props.data.title} keywords={this.props.data.keywords} description={this.props.data.description}/>
			<TopBar/>
			<div className="container space-around">
			{post}
			</div>
			</>
		)
	}
}

export default Post
