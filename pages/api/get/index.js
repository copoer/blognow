/* Get Muliple Blog Posts */
const utils = require('../../../core/utils.js');
const mongodb = require('../../../core/mongodb.js');

//Capturing endpoint
export default (req, res) => {
	const test = utils.invalidJSON(req.body);
	if (test) { //Invalid document
		res.setHeader('Content-Type', 'application/json');
		res.statusCode = 400;
		res.end(JSON.stringify({ error: test }));
	} else { //Valid document, Create post
		const params = JSON.parse(req.body.toString());
		let query = {};
		let limit = 25;
		let page = 1;
		if (params.query) {
			let str = new RegExp(".*" + params.query.toLowerCase() + ".*", "i");
			query = {$or: [{"title":{$regex:str}}, {"markdown":{$regex:str}}, {"keywords":{$regex:str}}]};
		}
		if (params.limit) {
			limit = params.limit;
		}
		if (params.page) {
			page = params.page;
		}
		mongodb.getPosts(query, limit, page, (data, err) => {
			res.setHeader('Content-Type', 'application/json');
			if (err) {
				res.statusCode = 400;
				res.end(JSON.stringify({ error: err }));
			} else {
				res.statusCode = 200;
				res.end(JSON.stringify({data}));
			}
		});
	}
};
