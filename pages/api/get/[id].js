/* Get Blog Post */
const mongodb = require('../../../core/mongodb.js');
const ObjectId = require('mongodb').ObjectID;

//Capturing endpoint
export default (req, res) => {
	res.setHeader('Content-Type', 'application/json');
	const url = req.query.id;
	if (url) {
		const query = { url: url };
		mongodb.getPost(query, (data, err) => {
			if (err) {
				res.statusCode = 400;
				res.end(JSON.stringify({ error: err }));
			} else {
				// Increment Page Count
				if (data !== null) {
					if (!data.pageCount) data.pageCount = 0;
					data.pageCount++;
					mongodb.replacePost({_id: new ObjectId(data.id)}, data, (result, error) => {
						console.log(result);
						if (error) {
							res.statusCode = 400;
							res.end(JSON.stringify({ error: err }));
						}
						res.statusCode = 200;
						res.end(JSON.stringify({data}));
					});
				} else {
					res.statusCode = 200;
					res.end(JSON.stringify({data}));
				}
			}
		});
	} else {
		res.statusCode = 400;
		res.end(JSON.stringify({ error: "Must Provide Id" }));
	}
};
