/* Create New Blog Post */
	const utils = require('../../../core/utils.js');
const mongodb = require('../../../core/mongodb.js');

//Capturing endpoint
export default (req, res) => {
	utils.auth(req, res, (req, res, token) => {
		const test = utils.invalidDocument(req.body);
		if (test) { //Invalid document
			res.setHeader('Content-Type', 'application/json');
			res.statusCode = 400;
			res.end(JSON.stringify({ error: test }));
		} else { //Valid document, Create post
			const obj = JSON.parse(req.body.toString());
			obj.author = token.username;
			// Append date
			obj.dataType = "post";
			obj.date = Date.now();
			mongodb.createPost(obj, (data, err) => {
				res.setHeader('Content-Type', 'application/json');
				if (err) {
					res.statusCode = 400;
					res.end(JSON.stringify({ error: err }));
				} else {
					res.statusCode = 200;
					res.end(JSON.stringify({data}));
				}
			});
		}
	});
};
