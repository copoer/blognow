/* Replace Existing Blog Post */
	const utils = require('../../../core/utils.js');
const mongodb = require('../../../core/mongodb.js');
const ObjectId = require('mongodb').ObjectID;

//Capturing endpoint
export default (req, res) => {
	utils.auth(req, res, (req, res, token) => {
		const test = utils.invalidDocument(req.body);
		if (test) { //Invalid document
			res.setHeader('Content-Type', 'application/json');
			res.statusCode = 400;
			res.end(JSON.stringify({ error: test }));
		} else { //Valid document, Create post
			res.setHeader('Content-Type', 'application/json');
			const obj = JSON.parse(req.body.toString());
			const id = req.query.id;
			obj.author = token.username;
			obj.dataType = "post";
			// Append current time
			obj.date = Date.now();
			if (id) {
				const query = { _id: new ObjectId(id) };
				mongodb.replacePost(query, obj, (data, err) => {
					if (err) {
						res.statusCode = 400;
						res.end(JSON.stringify({ error: err }));
					} else {
						res.statusCode = 200;
						res.end(JSON.stringify({ data }));
					}
				});
			} else {
				res.statusCode = 400;
				res.end(JSON.stringify({ error: "Must Provide Id" }));
			}
		}
	});
};
