/* Delete Blog Post */
const utils = require('../../../core/utils.js');
const mongodb = require('../../../core/mongodb.js');
const ObjectId = require('mongodb').ObjectID;

//Capturing endpoint
export default (req, res) => {
	utils.auth(req, res, (req, res) => {
		res.setHeader('Content-Type', 'application/json');
		const id = req.query.id;
		if (id) {
			const query = { _id: new ObjectId(id) };
			mongodb.deletePost(query, (data, err) => {
				if (err) {
					res.statusCode = 400;
					res.end(JSON.stringify({ error: err }));
				} else {
					res.statusCode = 200;
					res.end(JSON.stringify({data}));
				}
			});
		} else {
			res.statusCode = 400;
			res.end(JSON.stringify({ error: "Must Provide Id" }));
		}
	});
};
