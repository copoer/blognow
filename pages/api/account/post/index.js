/* Create New Account */
	const utils = require('../../../../core/utils.js');
const mongodb = require('../../../../core/mongodb.js');
const bcrypt = require('bcrypt');
const accountCreation = process.env.accountCreation;

//Capturing endpoint
export default (req, res) => {
	const test = utils.invalidJSON(req.body);
	res.setHeader('Content-Type', 'application/json');
	if (!accountCreation) { //Account Creation is Disabled
		res.statusCode = 400;
		res.end(JSON.stringify({ error: "You cannot create and account at this time" }));
	} else if (test) { //Invalid JSON
		res.statusCode = 400;
		res.end(JSON.stringify({ error: test }));
	} else { //Valid JSON, Create Account
		const obj = JSON.parse(req.body.toString());
		if (!obj || !obj.username || !obj.password) {
			res.statusCode = 400;
			res.end(JSON.stringify({ error: "Password and Username must be set" }));
		} else {
			//Check for existing account
			let query = {username: obj.username};
			mongodb.getAccount(query, (data, err) => {
				if (err) {
					res.statusCode = 400;
					res.end(JSON.stringify({ error: err }));
				} else {
					if (!data) {
						//Hash password
						bcrypt.hash(obj.password, 10, function(err, hash) {
							obj.password = hash;
							//Account doesn't exist
							mongodb.createAccount(obj, (data, err) => {
								if (err) {
									res.statusCode = 400;
									res.end(JSON.stringify({ error: err }));
								} else {
									res.statusCode = 200;
									res.end(JSON.stringify({data}));
								}
							});
						});
					} else {
						res.statusCode = 400;
						res.end(JSON.stringify({ error: "Account Exsits" }));
					}
				}
			});
		}
	}
};
