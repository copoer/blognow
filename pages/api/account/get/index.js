/* Get Account, Check password, generate cookie */
const utils = require('../../../../core/utils.js');
const mongodb = require('../../../../core/mongodb.js');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const jwtSecret = process.env.jwtSecret.toString();

//Capturing endpoint
export default (req, res) => {
	const test = utils.invalidJSON(req.body);
	res.setHeader('Content-Type', 'application/json');
	if (test) { //Invalid JSON
		res.statusCode = 400;
		res.end(JSON.stringify({ error: test }));
	} else { //Valid JSON, Create Account
		const obj = JSON.parse(req.body.toString());
		if (!obj.username || !obj.password) {
			res.statusCode = 400;
			res.end(JSON.stringify({ error: "Password and Username must be set" }));
		} else {
			//Check for existing account
			let query = {username: obj.username};
			mongodb.getAccount(query, (data, err) => {
				if (err) {
					res.statusCode = 400;
					res.end(JSON.stringify({ error: err }));
				} else if (data !== null) {
					//Verify Password
					bcrypt.compare(obj.password, data.password, function(err, test) {
						if (test) {
							//Generate Token
							const token = jwt.sign({ "username": obj.username }, jwtSecret, { expiresIn: 86400 }) // 24 hour token
							res.statusCode = 200;
							res.end(JSON.stringify({token}));
						} else {
							res.statusCode = 400;
							res.end(JSON.stringify({ error: "Invalid Login" }));
						}
					});
				} else {
					res.statusCode = 400;
					res.end(JSON.stringify({ error: "Invalid Login" }));
				}
			});
		}
	}
};
