/* Index Page */
import Header from '../../components/Header';
import TopBar from '../../components/TopBar';
import PostList from '../../components/PostList';
import Paging from '../../components/Paging';
import fetch from 'isomorphic-unfetch';
import React from 'react'
import Error from 'next/error'
const host = process.env.host;

// Paging Information
class Index extends React.Component {
	static async getInitialProps(req) {
		let page = 0, numPerPage = 10, pageCount = 0, query = "", queryString = "";
		// Read Query Parameters
		if (Object.keys(req.query).length !== 0) {
			if (req.query.query && req.query.query !== "") {
				query = req.query.query;
				queryString = "&query=" + query;
			}
			if (req.query.page) {
				page = req.query.page;
			}
		}

		// Get Posts
		const dataRes = await fetch(host+'/api/get', {
			method: 'POST',
			body: JSON.stringify({query: query, page: page, limit: numPerPage})
		});
		const data = await dataRes.json();

		// Get Fall Back
		const fallbackRes = await fetch(host+'/api/get', {
			method: 'POST',
			body: JSON.stringify({limit: 3})
		});
		const fallbackData = await fallbackRes.json();


		// Return data or error
		if (data.error) {
			return {error: "500 Database Error"};
		} else {
			// Paging
			pageCount = Math.ceil(data.data.count/numPerPage);
			if (pageCount < 0) pageCount = 0;
			data.data.pageCount = pageCount;
			data.data.page = page;
			data.data.query = query;
			data.data.queryString = queryString;
			return {search:data.data,fallback:fallbackData.data};
		}
	}

	render() {
		if (this.props.error) return <Error statusCode={this.props.error} />;

		return (
			<div>
			<Header/>
			<TopBar query={this.props.search.query}/>
			<div className="flex-container column center">
			{this.props.search.count <= 0 && 
			<h2 className="background-fill simple-padding">No posts found. Check these pages:</h2>}
			<PostList data={this.props.search.count >= 1 ? this.props.search.data : this.props.fallback.data}/>
			<Paging page={this.props.search.page} pageCount={this.props.search.pageCount} queryString={this.props.search.queryString}/>
			</div>
			</div>
		)
	}
}


export default Index;
