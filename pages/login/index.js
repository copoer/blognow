/* Login Page */
import Header from '../../components/Header';
import TopBar from '../../components/TopBar';
import fetch from 'isomorphic-unfetch';
import React from 'react';
import { Cookies } from 'react-cookie';
import Router from 'next/router'
const host = process.env.host;

const cookies = new Cookies();
class Index extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.updateUsername = this.updateUsername.bind(this);
		this.updatePassword = this.updatePassword.bind(this);
		this.handleLogin = this.handleLogin.bind(this);
		this.handleCreate = this.handleCreate.bind(this);
		this.hideAlert = this.hideAlert.bind(this);
	}
	updateUsername(event) {
		this.setState({username: event.target.value});
	}
	updatePassword(event) {
		this.setState({password: event.target.value});
	}
	async handleLogin(event) {
		event.preventDefault();
		const account = this.state;
		// Delete meta data
		delete account.loginError;
		const dataRes = await fetch(host+'/api/account/get', {
			method: 'POST',
			body: JSON.stringify(account)
		});
		const data = await dataRes.json();
		if (data.error) {
			this.setState({loginError: data.error});
		} else {
			this.state = {};
			cookies.set('token', data.token, {path: '/'});
			Router.push('/');
		}
	}
	async handleCreate(event) {
		event.preventDefault();
		const account = this.state;
		// Delete meta data
		delete account.loginError;
		const dataRes = await fetch(host+'/api/account/post', {
			method: 'POST',
			body: JSON.stringify(account)
		});
		const data = await dataRes.json();
		if (data.error) {
			this.setState({loginError: data.error});
		} else {
			alert("Account Created");
		}
	}
	hideAlert() {
		this.setState({loginError: false});
	}
	render() {
		let alertMessage;
		if (this.state.loginError) {
			alertMessage = (
				<div className="alert">
				<span className="close-alert" onClick={this.hideAlert}>&times;</span>
				{this.state.loginError}
				</div>
			)
		}
		return (  
			<div>
			<Header title="Login" />
			<TopBar/>
			<div className="container background-fill">
			<h1>Login</h1>
			<div className="edit-post">
			{alertMessage}
			<form className="container post-form" action="">
			<h4>Username:</h4>
			<input className="form-item username" type="text" name="username" placeholder="Bob" value={this.state.username} onChange={this.updateUsername}/>
			<h4>Password:</h4>
			<input className="form-item password" type="password" name="password" placeholder="Password" value={this.state.password} onChange={this.updatePassword}/>
			<button className="form-item submit-button" onClick={this.handleLogin}>Login</button>
			<button className="form-item submit-button" onClick={this.handleCreate}>Create Account</button>
			</form>
			</div>
			</div>
			</div>
		)
	}
}
export default Index;
