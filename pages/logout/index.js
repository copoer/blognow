/* Logout Redirect Page */
import Link from 'next/link'
import React from 'react';
import Router from 'next/router'
import { Cookies } from 'react-cookie';

const cookies = new Cookies();
async function clear() {
	await cookies.remove('token', {path: '/'});
	Router.push('/');
}
class Index extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
		clear();
	}
	render() {
		return (
			<>
			</>
		);
	}
}
export default Index;
