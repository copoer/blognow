# [BlogNow](https://gitlab.com/copoer/blognow)


![](https://i.imgur.com/n5n2E4y.mp4)

- [Git Repository](https://gitlab.com/copoer/blognow)
- [Live Demo](https://alii.co/)

## Description

BlogNow provides a platform for blogging, journaling, note taking and more. The simple user interface allows you to create/edit documents written in markdown. The login system allows you to mange your blog server securely from anywhere. The simple and easy design means it is easy for developers to build and customise the app to their liking. The platform can also easily be deployed as a serverless application.


## Key Features:

- Simple clean minimal design 
- Multi-User Authentication
- Create/Edit/Delete blog posts
- Markdown supported
- Search through existing post
- SEO friendly URLs and keywords
- Easily deploys to serverless platforms
- Can be hosted for free

## Technologies:

- ReactJS
- NextJS
- NodeJS
- Mongodb
- JWT

---

## Documentation

- [Install Guide](https://copoer.gitlab.io/blognow/docs/install.html)
- [How to get free serverless hosting](https://copoer.gitlab.io/blognow/docs/freehosting.html)
- [Technical Docs](https://copoer.gitlab.io/blognow/docs/structure.html)
